// Copyright (C) 2019 "Maxime “pep” Buquet <pep@bouah.net>"
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
// for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

use crate::errors::Error;

use std::collections::{HashMap, HashSet};
use serde::{Serialize, Deserialize};
use rand::{seq::IteratorRandom, thread_rng};
use xmpp_parsers::BareJid;

pub type Action = Option<String>;

#[derive(Debug)]
pub enum Transition {
    /// Initiate session. This means there was no voting entry for the user at all. There is a
    /// slight difference with `StartSession` in that this records the fact that the user wants to
    /// participate in the vote.
    InitSession,

    /// Start session. There was an empty entry for the user. This actually starts the voting
    StartSession,

    /// Cancel a session for a user. Removes every vote for that user.
    CancelSession,

    /// Votes 'yes' for an candidate
    ApproveCandidate,

    /// Votes 'no' for an candidate
    OpposeCandidate,

    // /// Votes 'abstain' for an candidate
    // AbstainCandidate(Candidate),
}

// enum Action { Message(Message), AdHoc(Iq), .. }
// pub fn handle_transition(vote: Vote, transition: Transision) -> (Vote, Action) {

#[derive(Debug, Eq, PartialEq, Clone, Hash, Serialize, Deserialize)]
#[serde(transparent)]
pub struct Candidate {
    pub fullname: String,
}

#[derive(Debug, PartialEq, Clone, Serialize, Deserialize)]
pub enum VotingState {
    /// Voter has started a voting session.
    Init,

    /// Voter has been proposed to vote for this candidate. Alongside with past votes.
    Voting {
        candidate: Candidate,
        votes: HashMap<Candidate, bool>,
    },

    /// Voter has issued a vote for all candidates.
    Completed(HashMap<Candidate, bool>),
}

#[derive(Debug, PartialEq, Clone, Serialize, Deserialize)]
pub struct Vote {
    pub date: String,
    pub candidates: HashSet<Candidate>,
    state: HashMap<BareJid, VotingState>,
}

fn _next_candidate(
    all_candidates: &HashSet<Candidate>,
    voted_candidates: &HashSet<Candidate>,
) -> Option<Candidate> {
    let mut rng = thread_rng();
    let diff = all_candidates.difference(voted_candidates).cloned();
    diff.choose(&mut rng)
}

impl<'a> Vote {
    pub fn new() -> Vote {
        let foo = Candidate {
            fullname: String::from("Foo"),
        };
        let bar = Candidate {
            fullname: String::from("Bar"),
        };
        let meh = Candidate {
            fullname: String::from("Meh"),
        };

        let mut candidates = HashSet::new();
        candidates.insert(foo);
        candidates.insert(bar);
        candidates.insert(meh);

        Vote {
            date: String::from("2019-10-01"),
            candidates,
            state: HashMap::new(),
        }
    }

    pub fn init(mut self, from: BareJid) -> Vote {
        self.state.insert(from, VotingState::Init);
        self
    }

    pub fn get(&'a self, from: &BareJid) -> Option<&'a VotingState> {
        self.state.get(from)
    }

    pub fn remove(mut self, from: &BareJid) -> Vote {
        self.state.remove(from);
        self
    }

    fn _add_vote(self, from: &BareJid, result: bool) -> Result<(Vote, Action), Error> {
        let vote = self.insert(from.clone(), result);
        let mut msg = String::from("You have completed the vote.");
        if let Some(next) = vote.get_next_candidate(from) {
            msg = format!("Approve: {}? (yes/no)", &next.fullname);
        }

        Ok((vote, Some(msg)))
    }

    pub fn next(mut self, from: &BareJid, transition: Transition) -> Result<(Vote, Action), Error> {
        let vote_proceed = format!(
            r#"Hi, {}!
    Voting has begun for: XSF Membership.
    By proceeding, you affirm that you wish to have your vote count as a proxy vote in the official meeting to be held on {} in {}.
    Would you like to cast your votes now? (yes / no)"#,
            from.clone(),
            self.date,
            "xsf@muc.xmpp.org"
        );

        match transition {
            Transition::InitSession => {
                let vote = self.init(from.clone());
                Ok((vote, Some(vote_proceed)))
            }
            Transition::StartSession => {
                // XXX: ATM there shouldn't be any session without candidates. And if there is,
                // panic anyway.
                let next = self.set_next_candidate(HashMap::new()).unwrap();
                self.state.insert(from.clone(), VotingState::Voting {
                    candidate: next.clone(),
                    votes: HashMap::new(),
                });
                let msg = format!("Approve: {}? (yes/no)", &next.fullname);

                Ok((self, Some(msg)))
            }
            Transition::CancelSession => {
                let vote = self.remove(from);
                Ok((vote, None))
            }
            Transition::ApproveCandidate => {
                self._add_vote(from, true)
            }
            Transition::OpposeCandidate => {
                self._add_vote(from, false)
            }
        }
    }

    pub fn insert(mut self, from: BareJid, vote: bool) -> Vote {
        let state = self.state.get(&from);

        // TODO: Error if else?
        if let Some(VotingState::Voting { candidate, votes }) = state {
            let mut votes = votes.clone();
            votes.insert(candidate.clone(), vote);

            if let Some(candidate) = self.set_next_candidate(votes.clone()) {
                self.state.insert(from, VotingState::Voting { candidate, votes });
            } else {
                self.state.insert(from, VotingState::Completed(votes));
            }
        }

        self
    }

    pub fn is_candidate(&self, candidate: &Candidate) -> bool {
        self.candidates.contains(candidate)
    }

    pub fn get_next_candidate(&self, from: &BareJid) -> Option<Candidate> {
        match self.state.get(from) {
            Some(VotingState::Voting { candidate, .. }) => Some(candidate.clone()),
            _ => None,
        }
    }

    pub fn set_next_candidate(&self, votes: HashMap<Candidate, bool>) -> Option<Candidate> {
        let voted_candidates: HashSet<_> = votes.iter()
            .map(|(c, _)| c)
            .cloned()
            .collect();
        _next_candidate(&self.candidates, &voted_candidates)
    }

    pub fn is_complete(&self, from: &BareJid) -> bool {
        match self.state.get(from) {
            Some(VotingState::Completed(_)) => true,
            _ => false
        }
    }

    pub fn to_string(&self) -> String {
        serde_json::to_string_pretty(self).unwrap()
    }

    pub fn from_string(s: &str) -> Vote {
        match serde_json::from_str(s) {
            Ok(vote) => vote,
            Err(err) => {
                eprintln!("Unable to deserialize state: {:?}. Generating new state.", err);
                Vote::new()
            }
        }
    }
}
