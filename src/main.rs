// Copyright (C) 2019 "Maxime “pep” Buquet <pep@bouah.net>"
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
// for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

mod errors;
mod handlers;
mod voting;

use crate::errors::Error;
use crate::handlers::{next_step};
use crate::voting::Vote;

use std::env::args;
use std::fs::File;
use std::io::{Read, Write};
use std::path::Path;
use std::process::exit;
use xmpp::{ClientBuilder, ClientFeature, ClientType, Event};
use xmpp_parsers::message::MessageType;

fn read_state(path: &str) -> String {
    let mut data = String::new();
    let mut file = match File::open(path) {
        Ok(file) => file,
        Err(_) => return String::new(),
    };
    file.read_to_string(&mut data).unwrap();
    data
}

fn write_state(path: &str, state: String) {
    let mut file = File::create(path).expect("Unable to open file");
    file.write_all(state.as_bytes()).unwrap();
}

#[tokio::main]
async fn main() -> Result<(), Option<()>> {
    env_logger::init();

    let args: Vec<String> = args().collect();
    if args.len() != 4 {
        println!("Usage: {} <jid> <password> <statefile>", args[0]);
        exit(1);
    }
    let jid = &args[1];
    let password = &args[2];
    let statefile = {
        let p = Path::new(&args[3]);
        p.to_str().unwrap()
    };

    // Client instance
    let mut agent = ClientBuilder::new(jid, password)
        .set_client(ClientType::Bot, "memberbot")
        .set_website("https://gitlab.com/xmpp-rs/xmpp-rs")
        .set_default_nick("bot")
        .enable_feature(ClientFeature::ContactList)
        .build()
        .unwrap();

    while let Some(events) = agent.wait_for_events().await {
        for evt in events {
            match evt {
                Event::Online => {
                    println!("Online.");
                }
                Event::Disconnected => {
                    println!("Disconnected.");
                    return Err(None);
                }
                Event::ChatMessage(from, body) => {
                    // TODO: ensure senders are known (in the roster).
                    let vote = {
                        let s = read_state(statefile);
                        Vote::from_string(s.as_str())
                    };
                    match next_step(&from, vote, body.0) {
                        Ok((new_vote, message)) => {
                            if let Some(msg) = message {
                                agent.send_message(from.clone().into(), MessageType::Chat, "en", &msg).await;
                            }
                            write_state(statefile, new_vote.to_string());
                        }
                        Err(err) => {
                            println!("Error: {:?}", err);
                            let msg = match err {
                                Error::InvalidCommand(c) => {
                                    format!("Unknown answer {:?}. Please try again.", c)
                                }
                                _ => String::from("An error occured, please try to reach the operator of this bot."),
                            };
                            agent.send_message(from.clone().into(), MessageType::Chat, "en", &msg).await;
                        }
                    }
                }
                _ => (),
            }
        }
    }

    Ok(())
}
