// Copyright (C) 2019 "Maxime “pep” Buquet <pep@bouah.net>"
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
// for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

use std::fmt;
use std::str::FromStr;

use crate::errors::Error;
use crate::voting::{Action, Transition, Vote, VotingState};
use xmpp_parsers::BareJid;

#[derive(Debug)]
enum Command {
    /// Answer 'yes' to the current question
    Yes,

    /// Answer 'no' to the current question
    No,

    // /// Answer 'abstain' to the current question
    // Abstain,

    // /// Cancel the current voting session as if it had not happened.
    // Cancel,

    // /// Stop the current voting session while allowing to resume it later on.
    // Stop,
}

impl FromStr for Command {
    type Err = Error;

    fn from_str(command: &str) -> Result<Self, Self::Err> {
        match command.trim().to_lowercase().as_ref() {
            "yes" => Ok(Command::Yes),
            "no" => Ok(Command::No),
            // "abstain" => Ok(Command::Abstain),
            c => Err(Error::InvalidCommand(String::from(c))),
        }
    }
}

impl fmt::Display for Command {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match *self {
            Command::Yes => write!(f, "yes"),
            Command::No => write!(f, "no"),
            // Command::Abstain => write!(f, "abstain"),
        }
    }
}

pub fn next_step(from: &BareJid, vote: Vote, body: String) -> Result<(Vote, Action), Error> {
    let _command_error = String::from("Please respond with yes or no.");
    let _vote_cancel = String::from("Your session has been canceled. You can start a voting session again later.");
    let _vote_ended = format!("Thank you for voting, {}! If you wish to recast your votes later, just start a new voting session.", from.clone());

    let command = Command::from_str(&body);

    match vote.get(&from) {
        None => {
            println!("No previous entry for this Jid.");
            vote.next(&from, Transition::InitSession)
        }
        Some(VotingState::Init) => {
            println!("There is an empty entry for this Jid.");

            match command {
                Err(err) => return Err(err),
                Ok(Command::No) => {
                    vote.next(&from, Transition::CancelSession)
                }
                Ok(Command::Yes) => {
                    vote.next(&from, Transition::StartSession)
                }
            }
        }
        Some(VotingState::Voting { .. }) => {
            match command {
                Err(err) => return Err(err),
                Ok(Command::Yes) => {
                    vote.next(&from, Transition::ApproveCandidate)
                }
                Ok(Command::No) => {
                    vote.next(&from, Transition::OpposeCandidate)
                }
            }
        }
        Some(VotingState::Completed(_)) => {
            Ok((vote, Some(String::from("You have completed the vote."))))
        }
    }
}
